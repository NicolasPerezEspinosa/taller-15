//importamos la clase express del archivo o libreria express
import express from 'express';
//creamos un objeto de la clase express
let app=express();
//con el metodo get, creamos la ruta /poesia y su funcion tipo flecha que nos muestra un poema
app.get("/poesia",(peticion,respuesta)=>{
    respuesta.send("Quiéreme día, quiéreme noche… ¡Y madrugada en la ventana abierta!… Si me quieres, no me recortes: ¡Quiéreme toda… O no me quieras.");
});
//con el metodo get, creamos la ruta /pasaje y su funcion tipo flecha que nos muestra un pasaje biblico
app.get("/pasaje",(peticion,respuesta)=>{
    respuesta.send("Salmos 34.18 El Señor está cerca de los quebrantados de corazón, y salva a los de espíritu abatido.");
});
//metodo listen para abrir el servidor en el puerto 3000, y una funcion tipo flecha para avisar que el servidor esta activo
app.listen(3000, ()=>{console.log("El servidor esta activo")});

//REFERENCIAS
//grabaciones de las clases: https://drive.google.com/file/d/1jtyJqy7caEOXQtBhcQW6rJXIg8UaNFHR/view?usp=sharing, y https://drive.google.com/file/d/1NP6MzALQm0t2g5MDgJtB6ZMtJChZ9IVQ/view?usp=sharing
//taller realizado anteriormente, ID del taller: TALLER 8